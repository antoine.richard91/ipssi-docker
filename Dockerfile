FROM alpine:3.17

# Installer PHP et les dépendances nécessaires
RUN apk add --no-cache php  php-pdo php-pdo_mysql

COPY ./index.php /var/www/php/index.php

# Exposer le port 3000 utilisé par 
EXPOSE 3000

# Lancer php sur le port 3000 lors du démarrage du conteneur
CMD php -S 0.0.0.0:3000 -t /var/www/php/