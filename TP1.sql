CREATE OR REPLACE DATABASE TP1;

USE TP1;

CREATE OR REPLACE TABLE users (
    id int UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
    firstname varchar(255)
);

INSERT INTO users(firstname) VALUES
("Christopher"),
("Michael"),
("Antoine");
