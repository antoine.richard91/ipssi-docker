<h1>Docker</h1>
<form method="POST">
    <label for="firstname">First name:</label>
    <input type="text" name="firstname" />
    <input type="submit" value="Add" />
</form>
<hr>

<?php
$server = "db:3306";
$username = "root";
$password = "secret";
$database = "TP1";

$conn = new PDO("mysql:host=$server;dbname=$database", $username, $password);

if (array_key_exists("firstname", $_POST)) {
    $query = $conn->prepare("INSERT INTO users(firstname) VALUES('{$_POST['firstname']}');");
    $query->execute();
}

$query = $conn->prepare("SELECT id, firstname FROM users");
$query->execute();
$query->setFetchMode(PDO::FETCH_ASSOC);

foreach ($query->fetchAll() as $k => $v) {
    echo "$k {$v['firstname']}<br>";
}
